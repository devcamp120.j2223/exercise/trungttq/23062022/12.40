import "bootstrap/dist/css/bootstrap.min.css"
import ContentComponent from "./components/content/contentComponents";
import ButtonComponent from "./components/button/button";

function App() {
  const submitButtonHandler = (event) => {
        
    console.log("form đã được submit");
    event.preventDefault();
}
  return (
    <div className="container jumbotron mt-5">
      <form onSubmit={submitButtonHandler}>
      <ContentComponent/>
      <ButtonComponent/>
      </form>
      
      
      
    </div>
  );
}

export default App;
