import { Component } from "react";

class ButtonComponent extends Component {
    
    render() {
        return (
            <div>
                <div className="row">
                    <button className="btn btn-success w-25" type="submit" >Send Data</button>
                </div>
            </div>
        )
    }
}
export default ButtonComponent;