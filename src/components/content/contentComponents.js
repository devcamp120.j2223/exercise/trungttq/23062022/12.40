import { Component } from "react";
import Firstname from "./firstname";
import Lastname from "./lastname";
import Country from "./country";
import Subject from "./subject";

class ContentComponent extends Component {
    render() {
        return (
            <div>
                <Firstname/>
                <Lastname/>
                <Country/>
                <Subject/>
            </div>
        )
    }
}
export default ContentComponent;