import { Component } from "react";

class Lastname extends Component {
    inputChangeHandler(event){
        console.log(event.target.value);
    }
    render() {
        return (
            <div className="row mb-2">
                <div className="col-4">
                    <label className="form-label">Lastname</label>
                </div>
                <div className="col-8">
                    <input className="form-control" placeholder="lastname" onChange={this.inputChangeHandler}/>
                </div>
            </div>
        )
    }
}
export default Lastname;