import { Component } from "react";

class Subject extends Component {
    inputChangeHandler(event) {
        console.log(event.target.value);
    }
    render() {
        return (
            <div className="row mb-2">
                <div className="col-4">
                    <label className="form-label">Subject</label>
                </div>
                <div className="col-8">
                    <textarea className="form-control" placeholder="write something" onChange={this.inputChangeHandler} />
                </div>
            </div>
        )
    }
}
export default Subject;