import { Component } from "react";

class Country extends Component {
    selectChangeHandler(event){
        console.log(event.target.value);
    }
    render() {
        return (
            <div className="row mb-2">
                <div className="col-4">
                    <label className="form-label">Country</label>
                </div>
                <div className="col-8">
                    <select className="form-control" onChange={this.selectChangeHandler}>
                        <option>Australia</option>
                        <option>US</option>
                        <option>UK</option>
                    </select>
                </div>
            </div>
        )
    }
}
export default Country;